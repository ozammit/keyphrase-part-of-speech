#!/usr/bin/env python3

# The path from where the datasets will be collected
# Change this according to the enviroment.
DATA_FOLDER = "keyphrases/"

# The path where the dataset will be stored
OUTPUT_FILE = 'keyphrase_pos.csv'

import os
# Pandas to create dataframe
import pandas as pd
# Part of speech tagging
import nltk
from nltk.tag import pos_tag
from nltk.data import load
# Plotting library
from matplotlib import pyplot as plt
# Tabulate
from tabulate import tabulate

# NLTK dataset
nltk.download('averaged_perceptron_tagger')
nltk.download('tagsets')

# The dataframe that will be populated
# with data from the dataset files.
all_data = pd.DataFrame()

def getListOfFiles(dirName):
    """
    Get all files recursively from a folder.
    dirName is the directory from where the
    files will be collected.
    """
    listOfFile = os.listdir(dirName)
    allFiles = list()
    for entry in listOfFile:
        fullPath = os.path.join(dirName, entry)
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        elif(fullPath.endswith(".key")):
            allFiles.append(fullPath)
    return allFiles

# Get all files of type *key from the folder recursively.
print(">> Get list of files.")
files = getListOfFiles(DATA_FOLDER)

# Loop through each file and get the contents
for file in files:
    print(f">> Reading contents of file {file}.")
    # Create a temporary dataframe
    # and populate it with the data from the
    # current file.
    df = pd.read_table(file, header=None, names=['word'])
    df['file'] = file
    # Concatenate the temporary dataframe
    # with all_data
    all_data = pd.concat([all_data, df])

# Drop rows having null values.
print(">> Remove rows having null values")
all_data = all_data.dropna()

# Reset indexes
print(">> Reset indexes.")
all_data = all_data.reset_index(drop=True)

# Delete unused data to free up some memory.
del files

print(">> Start data normalisation.")
# Convert words to lower
all_data['word'] = [x.lower() for x in all_data['word']]
# Split words into tokens
all_data['tokens'] = [x.split() for x in all_data['word']]
# Extract part-of-speech tags
all_data['tokens'] = [[y[1] for y in pos_tag(x)] for x in all_data['tokens']]
# All pos tags as dictionary
tagdict = load('help/tagsets/upenn_tagset.pickle')
# Create a column in the DataFrame for each POS tag.
for tag in tagdict.keys():
    all_data[tag] = [sum([1 for y in x if y==tag]) for x in all_data['tokens']]
# Remove columns that sum up to 0
drop_cols = all_data.columns[(all_data == 0).sum() == all_data.shape[0]]
all_data.drop(drop_cols, axis = 1, inplace = True)

# Remove tokens columns
del all_data['tokens']

print(f">> Total number of files: {len(all_data['file'].unique())}")
print(f">> Total number of unique words: {len(all_data['word'].unique())}")

# Compute totals
total_data = all_data[all_data.columns[~all_data.columns.isin(['word','file'])]]
total_data = total_data.sum()
# Get POS tags as labels
labels = [x for x,y in total_data.items() if y > 200]
# Get word count for each POS tag
values = [y for x,y in total_data.items() if y > 200]
fig, ax = plt.subplots(figsize=(15,7))

print(">> Plot result.")
# Plot line
ax.plot(values, color='lightgrey', linestyle='--')
# Plot scatter plot
ax.scatter(labels, values, color='green', s=100)
# Set y label
ax.set_ylabel("Word Count")
# Show grid
ax.grid()
print(">> Saving graph to image.")
plt.savefig('graph.png', bbox_inches='tight')

# Print table
rows = []
for label in labels:
    rows.append([label, tagdict[label][0], total_data[label]])
rows.sort(key=lambda x: x[-1], reverse=True)
print(">> Saving table to file.")
f = open('table.txt', 'w')
f.write(tabulate(rows,['Tag', 'Description', 'Quantity'], tablefmt="fancy_grid"))
f.close()

print(">> Saving dataset to CSV.")
# Export csv file
if os.path.exists(OUTPUT_FILE):
    os.remove(OUTPUT_FILE)
all_data.to_csv(OUTPUT_FILE, index=False)
