# Keyphrase Part of Speech Analysis

## Introduction

The aim of this project is to identify the most used part-of-speech tags by authors in keyphrases. In order to achieve this, we downloaded some datasets that were used by other studies to extract keyphrases. The *data* folder contains key files containing key phrases that were identified by authors from different texts.
The implementation in this project will extract such keywords and perform part-of-speech tagging using the [Python Natural Language Toolkit](https://www.nltk.org/). Once tags are identified the count of each tag is computed and a dataset containing the key phrases and their respective tag counts will be exported as CSV.

To clone to this project use:

~~~
git clone https://ozammit@bitbucket.org/ozammit/keyphrase-part-of-speech.git
~~~

The following list shows the dataset sources:

**hulth2003**: Hulth, A. (2003). Improved Automatic Keyword Extraction given More Linguistic Knowledge. Proceedings of the 2003 Conference on Empirical Methods in Natural Language Processing, 216–223.

**gollapalli2014**: Gollapalli, S. Das, & Caragea, C. (2014). Extracting keyphrases from research papers using citation networks. Twenty-Eighth AAAI Conference on Artificial Intelligence.

**krapivin2009**: Krapivin, M., Autaeu, A., & Marchese, M. (2009). Large dataset for keyphrases extraction.

**nguyen2007**: Nguyen, T. D., & Kan, M.-Y. (2007). Keyphrase Extraction in Scientific Publications. In D. H.-L. Goh, T. H. Cao, I. T. Sølvberg, & E. Rasmussen (Eds.), Asian Digital Libraries. Looking Back 10 Years and Forging New Frontiers (pp. 317–326). Springer Berlin Heidelberg.

**schutz2008**: Schutz, A. T., & others. (2008). Keyphrase extraction from single documents in the open domain exploiting linguistic and statistical methods. Masters of Applied Science M. App. Sc.


**kim2010**: Kim, S. N., Medelyan, O., Kan, M.-Y., & Baldwin, T. (2010). SemEval-2010 Task 5: Automatic Keyphrase Extraction from Scientific Articles. Proceedings of the 5th International Workshop on Semantic Evaluation, 21–26.


**aquino2015**: Aquino, G. O., & Lanzarini, L. C. (2015). Keyword identification in spanish documents using neural networks. Journal of Computer Science & Technology, 15.


**witten2005**: Witten, I. H., Paynter, G. W., Frank, E., Gutwin, C., & Nevill-Manning, C. G. (2005). KEA: Practical Automatic Keyphrase Extraction. In Design and Usability of Digital Libraries: Case Studies in the Asia Pacific. IGI global.

## Library Requirements


- pip install pandas
- pip install nltk
- pip install matplotlib
- pip install tabulate

## Implementation


```python
# The path from where the datasets will be collected
# Change this according to the enviroment.
DATA_FOLDER = "keyphrases/"

# The path where the dataset will be stored
OUTPUT_FILE = 'keyphrase_pos.csv'
```

### Import Required Libraries


```python
# To get all files from the data folder
import os
# Pandas to create dataframe
import pandas as pd
# Part of speech tagging
from nltk.tag import pos_tag
from nltk.data import load
# Plotting library
from matplotlib import pyplot as plt
# Tabulate
from tabulate import tabulate
```

### Get all data into DataFrame
The key files will be loaded recursively from the dataset path and loaded into a Pandas [DataFrame](https://pandas.pydata.org/pandas-docs/stable/reference/api/pandas.DataFrame.html).


```python
%%time

# The dataframe that will be populated
# with data from the dataset files.
all_data = pd.DataFrame()

def getListOfFiles(dirName):
    """
    Get all files recursively from a folder.
    dirName is the directory from where the
    files will be collected.
    """
    listOfFile = os.listdir(dirName)
    allFiles = list()
    for entry in listOfFile:
        fullPath = os.path.join(dirName, entry)
        if os.path.isdir(fullPath):
            allFiles = allFiles + getListOfFiles(fullPath)
        elif(fullPath.endswith(".key")):
            allFiles.append(fullPath)
    return allFiles

# Get all files of type *key from the folder recursively.
files = getListOfFiles(DATA_FOLDER)

# Loop through each file and get the contents
for file in files:
    # Create a temporary dataframe
    # and populate it with the data from the
    # current file.
    df = pd.read_table(file, header=None, names=['word'])
    df['file'] = file
    # Concatenate the temporary dataframe
    # with all_data
    all_data = pd.concat([all_data, df])  

# Drop rows vaving null values.
all_data = all_data.dropna()

# Reset indexes
all_data = all_data.reset_index(drop=True)

# Delete unused data to free up some memory.
del files
```

    CPU times: user 56.2 s, sys: 3.32 s, total: 59.5 s
    Wall time: 1min 2s


### Tokenisation
Do some other processing on the data. Convert the keyphrases to lower case and split each keyphrase into tokens.
After tokenisation part-of-speech is performed using [pos_tag](https://www.nltk.org/api/nltk.tag.html) function from the [NLTK](https://www.nltk.org/) library.


```python
%%time
# Convert words to lower
all_data['word'] = [x.lower() for x in all_data['word']]
# Split words into tokens
all_data['tokens'] = [x.split() for x in all_data['word']]
# Extract part-of-speech tags
all_data['tokens'] = [[y[1] for y in pos_tag(x)] for x in all_data['tokens']]
```

    CPU times: user 17.1 s, sys: 461 ms, total: 17.6 s
    Wall time: 17.8 s


### Get all possible part-of-speech tags


```python
# All pos tags as dictionary
tagdict = load('help/tagsets/upenn_tagset.pickle')
```


```python
%%time
# Create a column in the DataFrame for each POS tag.
for tag in tagdict.keys():
    all_data[tag] = [sum([1 for y in x if y==tag]) for x in all_data['tokens']]
```

    CPU times: user 2.68 s, sys: 4.05 ms, total: 2.68 s
    Wall time: 2.71 s


### Re-structure the data
For clarity, since there are a lot of part-of-speech tags, we are removing empty columns.


```python
# Remove columns that sum up to 0
drop_cols = all_data.columns[(all_data == 0).sum() == all_data.shape[0]]
all_data.drop(drop_cols, axis = 1, inplace = True)

# Remove tokens columns
del all_data['tokens']
```

### Summary


```python
print(f"Total number of files: {len(all_data['file'].unique())}")
print(f"Total number of unique words: {len(all_data['word'].unique())}")
```

    Total number of files: 8412
    Total number of unique words: 34925


### Plot Top Tags


```python
# Compute totals
total_data = all_data[all_data.columns[~all_data.columns.isin(['word','file'])]]
total_data = total_data.sum()

# Get POS tags as labels
labels = [x for x,y in total_data.items() if y > 200]

# Get word count for each POS tag
values = [y for x,y in total_data.items() if y > 200]

fig, ax = plt.subplots(figsize=(15,7))

# Plot line
ax.plot(values, color='lightgrey', linestyle='--')

# Plot scatter plot
ax.scatter(labels, values, color='green', s=100)

# Set y label
ax.set_ylabel("Word Count")

# Show grid
ax.grid()

# Show plot
plt.show()
```

![png](graph.png)


### Display Top Tags Total


```python
rows = []

for label in labels:
    rows.append([label, tagdict[label][0], total_data[label]])

rows.sort(key=lambda x: x[-1], reverse=True)

print(tabulate(rows,['Tag', 'Description', 'Quantity'], tablefmt="fancy_grid"))
```

    ╒═══════╤══════════════════════════════════════════════╤════════════╕
    │ Tag   │ Description                                  │   Quantity │
    ╞═══════╪══════════════════════════════════════════════╪════════════╡
    │ NN    │ noun, common, singular or mass               │      67588 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ JJ    │ adjective or numeral, ordinal                │      20071 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ NNS   │ noun, common, plural                         │      16584 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ VBG   │ verb, present participle or gerund           │       3506 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ IN    │ preposition or conjunction, subordinating    │       1825 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ VBN   │ verb, past participle                        │       1636 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ FW    │ foreign word                                 │       1450 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ CD    │ numeral, cardinal                            │        580 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ RB    │ adverb                                       │        564 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ VBP   │ verb, present tense, not 3rd person singular │        555 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ CC    │ conjunction, coordinating                    │        428 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ VBD   │ verb, past tense                             │        294 │
    ├───────┼──────────────────────────────────────────────┼────────────┤
    │ DT    │ determiner                                   │        269 │
    ╘═══════╧══════════════════════════════════════════════╧════════════╛


### Export Data as CSV


```python
if os.path.exists(OUTPUT_FILE):
    os.remove(OUTPUT_FILE)
all_data.to_csv(OUTPUT_FILE, index=False)
```
